# FPGA & ARM SoC FMC Carrier (FASEC) project
For more info see http://www.ohwr.org/projects/fasec

Test project to verify SoC PL (i.e. FPGA) I/O port mapping.
Some basic logic added so the design is synthesisable and implementable.

