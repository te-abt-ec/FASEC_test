----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2016 01:57:26 PM
-- Design Name: 
-- Module Name: fasec_top - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity fasec_top is
  generic(DIFFBUSWIDTH : positive := 33);
  port (
    -- signals from/to PS
    --ps100_clk_i        : in    std_logic;
    -- FMC1-2 signals
    FMC2_LA_P_b        : out   std_logic_vector (DIFFBUSWIDTH-1 downto 0);
    FMC2_LA_N_b        : out   std_logic_vector (DIFFBUSWIDTH-1 downto 0);
    FMC1_LA_P_b        : in    std_logic_vector (DIFFBUSWIDTH-1 downto 0);
    FMC1_LA_N_b        : in    std_logic_vector (DIFFBUSWIDTH-1 downto 0);
    -- bus of differential and single ended signals not possible
    FMC2_LA_N33_b      : out   std_logic;
    FMC2_LA_P33_b      : in    std_logic;
    FMC1_LA_N33_b      : out   std_logic;
    FMC1_LA_P33_b      : out   std_logic;
    FMC2_PRSNTM2C_n    : in    std_logic;
    FMC2_CLK0M2C_P     : in    std_logic;
    FMC2_CLK0M2C_N     : in    std_logic;
    FMC2_CLK0C2M_P     : out   std_logic;
    FMC2_CLK0C2M_N     : out   std_logic;
    FMC1_PRSNTM2C_n    : in    std_logic;
    FMC1_CLK0M2C_P     : in    std_logic;
    FMC1_CLK0M2C_N     : in    std_logic;
    FMC1_CLK0C2M_P     : out   std_logic;
    FMC1_CLK0C2M_N     : out   std_logic;
    -- non-FMC clock signals
    osc125_clk_i       : in    std_logic;
    fpga_clk_p         : in    std_logic;
    fpga_clk_n         : in    std_logic;
    clk20_vcxo         : in    std_logic;
    -- misc. signals bank 33-34-35
    modea              : in    std_logic_vector(3 downto 0);
    modeb              : in    std_logic_vector(3 downto 0);
    tempid_dq          : inout std_logic;
    pg_in0_n           : in    std_logic;
    pg_in1_n           :       std_logic;
    t_pll25dac_sync2_n : out   std_logic;
    t_pll25dac_sclk    : out   std_logic;
    t_pll25dac_din     : out   std_logic;
    t_pll25dac_sync1_n : out   std_logic;
    gp_pushbutton_n    : in    std_logic;
    fan_pwm            : out   std_logic;
    t_wr_txfault       : in    std_logic;
    t_wr_moddef0       : in    std_logic;
    t_wr_los           : in    std_logic;
    t_wr_txdisable     : out   std_logic;
    t_wr_rateselect    : out   std_logic;
    eeprom_sda         : inout std_logic;
    eeprom_scl         : out   std_logic;
    pl_pc_gpio0        : inout std_logic;
    pl_pc_gpio1        : inout std_logic;
    -- misc. signals bank 12-13
    fpga_scl1          : out   std_logic;
    fpga_sda1          : inout std_logic;
    dig_in1_n          : in    std_logic;
    dig_in2_n          : in    std_logic;
    dig_in3_n          : in    std_logic;
    dig_in4_n          : in    std_logic;
    line_en_pl         : out   std_logic;
    line_pl            : out   std_logic;
    t_wr_moddef2       : inout std_logic;  --sda
    t_wr_moddef1       : out   std_logic;  --scl
    col_pl_1           : out   std_logic;
    col_pl_2           : out   std_logic;
    col_pl_3           : out   std_logic;
    col_pl_4           : out   std_logic;
    dig_out1           : out   std_logic;
    dig_out2           : out   std_logic;
    dig_out3           : out   std_logic;
    dig_out4           : out   std_logic;
    dig_out5           : out   std_logic;
    dig_out6           : out   std_logic;
    watchdog_pl        : out   std_logic);
end fasec_top;

architecture rtl of fasec_top is
  signal s_fmc1_dios, s_fmc2_dios       : std_logic_vector(33 downto 0) := (others => '0');
  signal s_fmc1_clk0m2c, s_fmc1_clk0c2m : std_logic;
  signal s_fmc2_clk0m2c, s_fmc2_clk0c2m : std_logic;
  signal s_fpga_clk                     : std_logic;
begin

  --------------------------------------------------------------------------------
  -- Generate FMC1 IOs and clocks, 33 differential pairs (used as INPUTS)
  --------------------------------------------------------------------------------
  gen_iobuf_fmc1 : for i in 0 to DIFFBUSWIDTH-1 generate
    cmp_IBUFDS_fmc1 : IBUFDS
      generic map (
        DIFF_TERM    => true,           -- Differential Termination 
        IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
        IOSTANDARD   => "LVDS_25")
      port map (
        O  => s_fmc1_dios(i),           -- Buffer output
        I  => FMC1_LA_P_b(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => FMC1_LA_N_b(i));  -- Diff_n buffer input (connect directly to top-level port)
  end generate gen_iobuf_fmc1;
  cmp_IBUFDS_fmc1_clk0m2c : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS_25")
    port map (
      O  => s_fmc1_clk0m2c,             -- Buffer output
      I  => FMC1_CLK0M2C_P,  -- Diff_p buffer input (connect directly to top-level port)
      IB => FMC1_CLK0M2C_N);  -- Diff_n buffer input (connect directly to top-level port)
  cmp_OBUFDS_fmc1_clk0c2m : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25",          -- Specify the output I/O standard
      SLEW       => "FAST")             -- Specify the output slew rate
    port map (
      O  => FMC1_CLK0C2M_P,  -- Diff_p output (connect directly to top-level port)
      OB => FMC1_CLK0C2M_N,  -- Diff_n output (connect directly to top-level port)
      I  => s_fmc1_clk0c2m);            -- Buffer input 
  -- connect the clock input with output
  s_fmc1_clk0c2m <= s_fmc1_clk0m2c;

  --------------------------------------------------------------------------------
  -- Generate FMC2 IOs and clocks, 33 differential pairs (used as OUTPUTS)
  --------------------------------------------------------------------------------    
  gen_iobuf_fmc2 : for i in 0 to DIFFBUSWIDTH-1 generate
    -- IOBUFDS: Differential Bi-directional Buffer
    cmp_OBUFDS_fmc2 : OBUFDS
      generic map (
        IOSTANDARD => "LVDS",           -- Specify the output I/O standard
        SLEW       => "FAST")           -- Specify the output slew rate
      port map (
        O  => FMC2_LA_P_b(i),  -- Diff_p output (connect directly to top-level port)
        OB => FMC2_LA_N_b(i),  -- Diff_n output (connect directly to top-level port)
        I  => s_fmc2_dios(i));          -- Buffer input 
  end generate gen_iobuf_fmc2;
  cmp_IBUFDS_fmc2_clk0m2c : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS")
    port map (
      O  => s_fmc2_clk0m2c,             -- Buffer output
      I  => FMC2_CLK0M2C_P,  -- Diff_p buffer input (connect directly to top-level port)
      IB => FMC2_CLK0M2C_N);  -- Diff_n buffer input (connect directly to top-level port)
  cmp_OBUFDS_fmc2_clk0c2m : OBUFDS
    generic map (
      IOSTANDARD => "LVDS",             -- Specify the output I/O standard
      SLEW       => "FAST")             -- Specify the output slew rate
    port map (
      O  => FMC2_CLK0C2M_P,  -- Diff_p output (connect directly to top-level port)
      OB => FMC2_CLK0C2M_N,  -- Diff_n output (connect directly to top-level port)
      I  => s_fmc2_clk0c2m);            -- Buffer input 
  -- connect the clock input with output
  s_fmc2_clk0c2m <= s_fmc2_clk0m2c;

  --------------------------------------------------------------------------------
  -- Clock in (synchronise) inputs by dubble buffer, using several clocks
  --------------------------------------------------------------------------------    
  FMC1_LA_P33_b <= 'Z';
  -- bank 13
  p_buff_osc125 : process(osc125_clk_i)
    variable v_buff : std_logic_vector(DIFFBUSWIDTH-1 downto 0);
  begin
    if rising_edge(osc125_clk_i) then
      s_fmc2_dios(DIFFBUSWIDTH-1 downto 0) <= v_buff(DIFFBUSWIDTH-1 downto 0);
      v_buff(DIFFBUSWIDTH-1 downto 0)      := s_fmc1_dios(DIFFBUSWIDTH-1 downto 0);
      -- test some single ended standards
      FMC1_LA_N33_b                        <= not FMC2_LA_P33_b;
      FMC2_LA_N33_b                        <= FMC2_LA_P33_b;
      dig_out3                             <= dig_in1_n;
      dig_out2                             <= dig_in2_n;
      fpga_scl1                            <= fpga_sda1;
      line_pl                              <= dig_in2_n and dig_in1_n;
      watchdog_pl                          <= dig_in2_n and dig_in1_n;
      dig_out1                             <= dig_in2_n and dig_in1_n;
      dig_out4                             <= dig_in2_n and dig_in1_n;
      dig_out5                             <= dig_in2_n and dig_in1_n;
      dig_out6                             <= dig_in2_n and dig_in1_n;
    end if;
  end process;
  -- bank 12
  p_buff_fmc1_clk0m2c : process(s_fmc1_clk0m2c)
  begin
    if rising_edge(s_fmc1_clk0m2c) then
      col_pl_1     <= dig_in4_n;
      col_pl_2     <= dig_in3_n;
      col_pl_3     <= dig_in4_n;
      col_pl_4     <= dig_in3_n;
      t_wr_moddef1 <= t_wr_moddef2;
      line_en_pl <= t_wr_moddef2 or FMC1_PRSNTM2C_n;
    end if;
  end process;
  -- bank 33
  p_buff_fmc2_clk0m2c : process(s_fmc2_clk0m2c)
  begin
    if rising_edge(s_fmc2_clk0m2c) then
      t_pll25dac_sync1_n <= pg_in0_n;
      t_pll25dac_sync2_n <= pg_in1_n;
      t_pll25dac_din     <= modea(2) or modeb(0) or modea(3);
      t_pll25dac_sclk    <= pg_in0_n or FMC2_PRSNTM2C_n;
    end if;
  end process;
  -- bank 34, 35
  p_buff_fpga_clk : process(s_fpga_clk)
  begin
    if rising_edge(s_fpga_clk) then
      fan_pwm         <= gp_pushbutton_n or modeb(1) or modea(0) or modea(1) or modeb(2) or modeb(3);
      t_wr_rateselect <= t_wr_txfault;
      t_wr_txdisable  <= t_wr_txfault or t_wr_moddef0 or t_wr_los;
      eeprom_sda      <= pl_pc_gpio0;
      eeprom_scl      <= pl_pc_gpio1;
    end if;
  end process;
  -- bank 34
  p_buff_clk20_vcxo : process(clk20_vcxo)
  begin
    if rising_edge(clk20_vcxo) then
      tempid_dq <= modeb(3);
    end if;
  end process;

  --------------------------------------------------------------------------------
  -- input buffer for the 'FPGA_CLK' from WR
  --------------------------------------------------------------------------------
  cmp_IBUFDS_fpga_clk : IBUFDS
    generic map (
      DIFF_TERM    => true,             -- Differential Termination 
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD   => "LVDS")
    port map (
      O  => s_fpga_clk,                 -- Buffer output
      I  => fpga_clk_p,  -- Diff_p buffer input (connect directly to top-level port)
      IB => fpga_clk_n);  -- Diff_n buffer input (connect directly to top-level port)  
end rtl;
