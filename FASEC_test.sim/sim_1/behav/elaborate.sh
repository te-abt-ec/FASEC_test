#!/bin/bash -f
xv_path="/local/EDA/Xilinx/Vivado/2015.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 1fa2d25b1e8e44dfbf1b545f0d24dc86 -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot fasec_top_behav xil_defaultlib.fasec_top -log elaborate.log
